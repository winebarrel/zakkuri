require 'formula'

class Kakasi < Formula
  url 'http://kakasi.namazu.org/stable/kakasi-2.3.4.tar.gz'
  homepage 'http://kakasi.namazu.org/'
  md5 '4eff51aafbd56c9635791a20c03efa8f'

  def install
    system './configure', "--prefix=#{prefix}"
    system 'make'
    system 'make install'
  end

  def patches
    {:p0 => 'https://bitbucket.org/winebarrel/zakkuri/raw/master/etc/kakasi-osx.patch'}
  end
end
