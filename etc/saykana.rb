require 'formula'

class Saykana < Formula
  url 'http://ftp.vector.co.jp/pack/mac/art/sound/SayKana0111.pkg.zip'
  version '0111'
  homepage 'http://www.a-quest.com/quickware/saykana/'
  md5 'f4a2d41581b8320881e9aaa9c8228853'

  def install
    system 'tar xf SayKana0111.pkg/Contents/Archive.pax.gz'
    system "install -d #{prefix}/bin"
    system "install -m 755 SayKana #{prefix}/bin"
  end
end
