#!/usr/bin/env ruby
require 'xmpp4r'
require 'xmpp4r/muc'
require 'xmpp4r-patch'

module Zakkuri
  class Bot
    def initialize(config)
      @config = config
      @client = Jabber::Client.new(@config[:jid])
      @muc    = Jabber::MUC::SimpleMUCClient.new(@client)
      Jabber.debug = !!@config[:debug]
    end

    def connect
      @client.connect
      @client.auth(@config[:password])
      @client.send(Jabber::Presence.new.set_type(:available))

      @muc.on_message do |time, nick, text|
        begin
          if text =~ /\A@"([^"]+)"\s+(.+)\Z/m or text =~ /\A@([^\s]+)\s+(.+)\Z/m
            mention = $1
            message = $2
            say(message) if @config[:mentions].include?(mention)
          end
        rescue => e
          $stderr.puts e
          $stderr.puts e.backtrace if @config[:debug]
        end
      end

      @muc.join(@config[:xmpp_jid] + '/' + @config[:nick])
    end

    private
    def say(message)
      message = message.gsub(/'/, %!'"'"'!)
      system("echo '#{message}' | kakasi | SayKana > /dev/null")
    end
  end
end
