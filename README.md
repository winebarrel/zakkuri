# Zakkuri

Zakkuri is HipChat Bot that speak messages to oneself.

## Requirements

* OS X
* Homebrew

## Installation

```sh
git clone https://bitbucket.org/winebarrel/zakkuri.git
cd zakkuri
./setup.sh
```

## Usage

```sh
# see https://www.hipchat.com/account/xmpp
export ZAKKURI_USERNAME=12345_12345
export ZAKKURI_NICKNAME="Taro Yamada"
export ZAKKURI_ROOM_IDS=12345_room-a,12345_room-b,...
#export ZAKKURI_MENTIONS=your_mention,someone_mentions,...
bundle exec ./zakkuri
```
