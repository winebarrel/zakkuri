# see http://d.hatena.ne.jp/motsat/20130324/1364121107
require 'socket'
require 'rexml/source'
require 'openssl'

class TCPSocket
  def external_encoding
    Encoding::BINARY
  end
end

class REXML::IOSource
  alias_method :encoding_assign, :encoding=

  def encoding=(value)
    encoding_assign(value) if value
  end
end

class OpenSSL::SSL::SSLSocket
  def external_encoding
    Encoding::BINARY
  end
end
